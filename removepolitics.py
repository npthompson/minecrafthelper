from pathlib import Path
import os
import sys


DESCRIPTION = "No Politics"
SPLASHTEXT = r"""Dig, build, have fun!"""
METADATA = r'''{
    "pack":{
        "pack_format":1,
        "description":"''' + DESCRIPTION + r'''"
    }
}'''


class MinecraftFolderError(Exception):
    ''' This error gets raised if the Minecraft resource pack folder can't be found. '''
    def __init__(self, message):
        super(MinecraftFolderError,self).__init__(message)


def find_path():
    ''' Finds and returns the Minecraft path if possible.
    Should work on Windows, MacOS and any Linux/BSD.
    Raises a MinecraftFolderError if the path can't be found.'''
    
    platform = sys.platform
    p = None
    if platform.startswith("win"):
        p = Path(os.getenv("appdata")).joinpath(".minecraft","resourcepacks")
    elif platform.startswith("darwin"):
        p = Path.home().joinpath("Library","Application Support",
                                 "minecraft","resourcespacks")
    else:
        p = Path.home().joinpath(r".minecraft","resourcepacks")

    if p and p.exists():
        return p
    else: raise MinecraftFolderError("Unable to find your Minecraft resource packs folder: {}".format(p if p else "None"))
    
 
def create_paths():
    ''' Creat the paths for our resource pack.
    Invokes find_path() to get the minecraft path.
    Re-raises exceptions encountered while trying to create the paths
    returns a dictionary with "pack_root" being the root folder for the data pack,
    and "texts_root" being the folder for the splash text file.'''
    
    folders = [DESCRIPTION,"assets","minecraft","texts"]

    try: current_path = find_path()
    except MinecraftFolderError as e: raise e

    pack_root = current_path.joinpath(folders[0])
    
    for folder in folders:
        current_path = current_path.joinpath(folder)
        if not current_path.exists():
            try:
                current_path.mkdir()
            except Exception as e:
                print("** Unable to create the required path {} **".format(current_path))
                raise e

    return {"pack_root": pack_root, "texts_root": current_path}


def create_meta(path):
    ''' Create the required meta file for the pack.
    Raises a MinecraftFolderError if the folder doesn't exist.
    The first parameter should be the root folder for the resource pack.
    Returns the file path on success.'''

    file = Path(path).joinpath("pack.mcmeta")
    if not file.parent.exists():
        raise MinecraftFolderError("Resource pack folder doesn't exist: {}".format(file.parent))
    
    with open(file,"wb") as metafile:
        metafile.write(METADATA.encode("utf-8"))

    return file


def create_texts(path):
    ''' Creates the texts document of the resource pack.
    Raises a MinecraftFolderError if the folder doesn't exist.
    the first parameters should be the containing folder's path.
    Returns the path name on success. '''

    file = Path(path).joinpath("splashes.txt")

    if not file.parent.exists():
        raise MineacraftFolderError("splashes text folder doesn't exist: {}".format(file.parent))

    with open(file,"wb") as splashfile:
        splashfile.write(SPLASHTEXT.encode("utf-8"))

    return file


if __name__ == "__main__":
    print("Creating resource pack folders.\n")
    try:
        paths = create_paths()
    except MinecraftFolderError as e:
        print("Unable to find or create the folders for the resource pack.")
        print(e)
        exit(-1);

    print("Found paths:")
    for k,v in paths.items():
        print("\t",k,v)
    print("")
    
    print("Creating meta file...")
    create_meta(paths["pack_root"])
    print("Creating splash text...")
    create_texts(paths["texts_root"])
    print('\nAll done. Go to your game settings, resource packs, and add "{}"'.format(DESCRIPTION))
    print("Don't worry if the game says it's for an older version.")
    print("After doing this, restart minecraft.")

            
        
