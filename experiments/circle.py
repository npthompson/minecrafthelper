from PIL import Image, ImageDraw
from pathlib import Path
from math import sin,cos,pi
from random import randint

save_file = Path.home().joinpath("Documents","circletest {}.png".format(randint(0,999)))

WHITE = (255,255,255)
BLACK = (0,0,0)
RED = (255,0,0)
BLUE = (0,0,255)
GREEN = (0,255,0)
HALF = {"x":180,"y":180}
R = 100

img = Image.new("RGB",(HALF['x']*2,HALF['y']*2), WHITE)

for i in range(90):
	x = int(HALF["x"] + R * cos(i))
	y = int(HALF["y"] + R * sin(i))
	
	img.putpixel((x,y),BLUE)
	
img.save(save_file)
img.show()

