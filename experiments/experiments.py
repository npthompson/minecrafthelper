from PIL import Image, ImageDraw
import math
from math import pi, sin, cos, sqrt, ceil, floor
from pandas import DataFrame
from pathlib import Path

WHITE = (255,255,255)
BLACK = (0,0,0)
GRAY = (127,127,127)
CUBECOLOURS = [BLACK,GRAY]
WIDTH = 256
HEIGHT = WIDTH
CUBESIZE = 8
SAVEFILE = Path.home().joinpath("Documents","experiments_points.xlsx")
SAVEIMAGE = Path.home().joinpath("Documents","Experiments_points.png")

colour_counter = 0;
my_image = Image.new("RGB",(WIDTH,HEIGHT),WHITE)
draw = ImageDraw.Draw(my_image)


def place_cube(pos):
    x,y = pos
    if 0 > x > WIDTH-CUBESIZE:
        raise ValueError(f" X Coordinate {x} is outside of the drawing area.")
    if 0 > y > WIDTH-CUBESIZE:
        raise ValueError(f"Y Coordinate {y} is outside of the drawing area.")
            
    draw.rectangle([x,y,x+CUBESIZE,y+CUBESIZE],GRAY,BLACK,1)

def get_y(r,x):
    y = math.sqrt(r**2-x**2)
    return y

def round_to_cubesize(n):
    half_point = CUBESIZE/2
    return n - n%CUBESIZE

def alternate_colour():
    global colour_counter
    c = CUBECOLOURS[colour_counter]
    colour_counter = (colour_counter +1) % len(CUBECOLOURS) 
    return c

def gridsquare(r,i):
    return i*CUBESIZE/r

def draw_circle(r):
    centre = (WIDTH/2,HEIGHT/2)
    df = DataFrame()
    for i in range(1800):
        x = cos(i/10) * r * 8
        y = sin(i/10) * r * 8
        df.loc[i,'x'] = x
        df.loc[i,'y'] = y
        x += centre[0]
        df.loc[i,'xx'] = x
        y += centre[1]
        df.loc[i,'yy'] = y
        place_cube((x,y))
    df.to_excel(str(SAVEFILE))
    my_image.save(str(SAVEIMAGE))


'''def draw_circle(r):
    # A*A+B*B = C*C
    # A+A = C*C - B*B
    centre = (WIDTH/2,HEIGHT/2)
    df = DataFrame()
    for j in range(-1,2,2):
        for i in range(-r*10,r*10+1):
            xx = i/10
            yy = math.sqrt(r*r-xx*xx) * j
            xx *= 8
            yy *= 8
            df.loc[i,'x'] = xx
            df.loc[i,'y'] = yy
            xx += centre[0]
            yy += centre[1]
            df.loc[i,'xx'] = xx
            df.loc[i,'yy'] = yy
            place_cube((xx,yy))
    df.to_excel(str(SAVEFILE))    
'''    
    
draw_circle(8)
                   
my_image.show()
